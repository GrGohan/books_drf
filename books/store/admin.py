from django.contrib import admin

from store import models


@admin.register(models.Book)
class BookAdmin(admin.ModelAdmin):
    list_display = ('name', 'price')


@admin.register(models.UserBookRelation)
class UserBookRelationAdmin(admin.ModelAdmin):
    pass
