from django.contrib.auth.models import User
from rest_framework import serializers
from rest_framework.serializers import ModelSerializer

from store.models import Book, UserBookRelation


class BookReaderSerializer(ModelSerializer):
    class Meta:
        model = User
        fields = ['username', 'first_name', 'last_name']


class BookSerializer(ModelSerializer):
    annotated_likes = serializers.IntegerField(read_only=True)
    rating = serializers.DecimalField(max_digits=3, decimal_places=2, read_only=True)
    max_rating = serializers.IntegerField(read_only=True)
    min_rating = serializers.IntegerField(read_only=True)
    in_users_bookmarks = serializers.IntegerField(read_only=True)
    dicount_price = serializers.DecimalField(max_digits=10, decimal_places=2, read_only=True)
    owner_name = serializers.CharField(source='owner.username', default="", read_only=True)
    readers = BookReaderSerializer(many=True, read_only=True)

    class Meta:
        model = Book
        fields = ['id', 'name', 'price', 'author_name', 'annotated_likes', 'rating',
                  'in_users_bookmarks', 'min_rating', 'max_rating', 'discount_plan',
                  'dicount_price', 'owner_name', 'readers']


class UserBookRelationSerializer(ModelSerializer):
    class Meta:
        model = UserBookRelation
        fields = ['book', 'like', 'in_bookmarks', 'rate']
