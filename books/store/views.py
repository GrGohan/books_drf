from django.db.models import Count, Case, When, Min, Max, F
from django.shortcuts import render
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework.filters import SearchFilter, OrderingFilter
from rest_framework.mixins import UpdateModelMixin
from rest_framework.permissions import IsAuthenticated
from rest_framework.viewsets import ModelViewSet, GenericViewSet

from store.models import Book, UserBookRelation
from store.permissions import IsOwnerOrStaffOrReadOnly
from store.serializers import BookSerializer, UserBookRelationSerializer


class BookViewSet(ModelViewSet):
    queryset = Book.objects.all().annotate(
            annotated_likes=Count(Case(When(userbookrelation__like=True, then=1))),
            in_users_bookmarks=Count(Case(When(userbookrelation__in_bookmarks=True, then=1))),
            min_rating=Min('userbookrelation__rate'),
            max_rating=Max('userbookrelation__rate'),
            dicount_price=F('price') - F('price') * F('discount_plan') / 100.00,
            owner_name=F('owner__username')).select_related('owner').prefetch_related('readers').order_by('id')

    serializer_class = BookSerializer
    permission_classes = [IsOwnerOrStaffOrReadOnly]

    filter_backends = [DjangoFilterBackend, SearchFilter, OrderingFilter]
    filter_fields = ['price']
    search_fields = ['name', 'author_name']
    ordering_fields = ['price', 'author_name']

    def perform_create(self, serializer):
        serializer.validated_data['owner'] = self.request.user
        serializer.save()


class UserBooksRelationView(UpdateModelMixin, GenericViewSet):
    permission_classes = [IsAuthenticated]
    queryset = UserBookRelation.objects.all()
    serializer_class = UserBookRelationSerializer
    lookup_field = 'book'

    def get_object(self):
        obj, _ = UserBookRelation.objects.get_or_create(user=self.request.user,
                                                        book_id=self.kwargs['book'])
        return obj


def auth(request):
    print("IP Address for debug-toolbar: " + request.META['REMOTE_ADDR'])
    return render(request, 'oauth.html')
