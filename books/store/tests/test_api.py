import json

from django.contrib.auth.models import User
from django.db import connection
from django.db.models import Count, Case, When, Max, Min, F
from django.urls import reverse
from django.test.utils import CaptureQueriesContext
from rest_framework import status
from rest_framework.exceptions import ErrorDetail
from rest_framework.test import APITestCase

from store.models import Book, UserBookRelation
from store.serializers import BookSerializer


class BooksApiTestCase(APITestCase):
    def setUp(self):
        self.first_user = User.objects.create(username='test_username')
        self.second_user = User.objects.create(username='Second test_username')
        self.staff_user = User.objects.create(username='Staff test_username', is_staff=True)

        self.first_book = Book.objects.create(name='First Test Book', price=1000, author_name='First author',
                                              owner=self.first_user)
        self.second_book = Book.objects.create(name='Second Test Book', price=2050.00, author_name='Second author',
                                               owner=self.first_user)
        self.third_book = Book.objects.create(name='Third Test Book', price=1050.00, author_name='Third author',
                                              owner=self.first_user)
        self.fourth_book = Book.objects.create(name='Fourth Test Book about First author', price=3100.00,
                                               author_name='Fourth author', owner=self.first_user)

        UserBookRelation.objects.create(user=self.first_user, book=self.first_book, like=True, rate=5)

    def test_get_filter(self):
        url = reverse('book-list')
        response = self.client.get(url, data={'price': 1000})

        books = Book.objects.filter(id=self.first_book.id).annotate(
            annotated_likes=Count(Case(When(userbookrelation__like=True, then=1))),
            in_users_bookmarks=Count(Case(When(userbookrelation__in_bookmarks=True, then=1))),
            min_rating=Min('userbookrelation__rate'),
            max_rating=Max('userbookrelation__rate'),
            dicount_price=F('price') - F('price') * F('discount_plan') / 100.00)
        serializer_data = BookSerializer(books, many=True).data

        self.assertEqual(status.HTTP_200_OK, response.status_code)
        self.assertEqual(serializer_data, response.data)

    def test_get_search(self):
        url = reverse('book-list')
        response = self.client.get(url, data={'search': 'First author'})

        books = Book.objects.filter(id__in=[self.first_book.id, self.fourth_book.id]).annotate(
            annotated_likes=Count(Case(When(userbookrelation__like=True, then=1))),
            in_users_bookmarks=Count(Case(When(userbookrelation__in_bookmarks=True, then=1))),
            min_rating=Min('userbookrelation__rate'),
            max_rating=Max('userbookrelation__rate'),
            dicount_price=F('price') - F('price') * F('discount_plan') / 100.00).order_by('id')
        serializer_data = BookSerializer(books, many=True).data

        self.assertEqual(status.HTTP_200_OK, response.status_code)
        self.assertEqual(serializer_data, response.data)

    def test_get_ordering(self):
        url = reverse('book-list')
        response = self.client.get(url, data={'ordering': 'price'})

        books = Book.objects.all().annotate(
            annotated_likes=Count(Case(When(userbookrelation__like=True, then=1))),
            in_users_bookmarks=Count(Case(When(userbookrelation__in_bookmarks=True, then=1))),
            min_rating=Min('userbookrelation__rate'),
            max_rating=Max('userbookrelation__rate'),
            dicount_price=F('price') - F('price') * F('discount_plan') / 100.00).order_by('price')
        serializer_data = BookSerializer(books, many=True).data

        self.assertEqual(status.HTTP_200_OK, response.status_code)
        self.assertEqual(serializer_data, response.data)

    def test_list(self):
        url = reverse('book-list')
        with CaptureQueriesContext(connection) as queries:
            response = self.client.get(url)
            self.assertEqual(2, len(queries))

        books = Book.objects.all().annotate(
            annotated_likes=Count(Case(When(userbookrelation__like=True, then=1))),
            in_users_bookmarks=Count(Case(When(userbookrelation__in_bookmarks=True, then=1))),
            min_rating=Min('userbookrelation__rate'),
            max_rating=Max('userbookrelation__rate'),
            dicount_price=F('price') - F('price') * F('discount_plan') / 100.00).order_by('id')
        serializer_data = BookSerializer(books, many=True).data

        self.assertEqual(status.HTTP_200_OK, response.status_code)
        self.assertEqual(serializer_data, response.data)
        self.assertEqual(serializer_data[0]['rating'], '5.00')
        self.assertEqual(serializer_data[0]['annotated_likes'], 1)

    def test_retrieve(self):
        url = reverse('book-detail', args=(self.first_book.id,))
        response = self.client.get(url)

        books = Book.objects.filter(id=self.first_book.id).annotate(
            annotated_likes=Count(Case(When(userbookrelation__like=True, then=1))),
            in_users_bookmarks=Count(Case(When(userbookrelation__in_bookmarks=True, then=1))),
            min_rating=Min('userbookrelation__rate'),
            max_rating=Max('userbookrelation__rate'),
            dicount_price=F('price') - F('price') * F('discount_plan') / 100.00).first()
        serializer_data = BookSerializer(books).data

        self.assertEqual(status.HTTP_200_OK, response.status_code)
        self.assertEqual(serializer_data, response.data)
        self.assertEqual(serializer_data['rating'], '5.00')
        self.assertEqual(serializer_data['annotated_likes'], 1)

    def test_create(self):
        self.assertEqual(4, Book.objects.all().count())

        url = reverse('book-list')
        json_data = json.dumps({
            "name": "Created Test Book",
            "price": 1001,
            "author_name": "First author"
        })
        self.client.force_login(self.first_user)
        response = self.client.post(url, data=json_data, content_type='application/json')

        self.assertEqual(status.HTTP_201_CREATED, response.status_code)
        self.assertEqual(5, Book.objects.all().count())
        self.assertEqual(self.first_user, Book.objects.last().owner)

    def test_update(self):
        url = reverse('book-detail', args=(self.first_book.id,))
        json_data = json.dumps({
            "name": self.first_book.name,
            "price": 1230,
            "author_name": self.first_book.author_name
        })
        self.client.force_login(self.first_user)
        response = self.client.put(url, data=json_data, content_type='application/json')

        self.assertEqual(status.HTTP_200_OK, response.status_code)
        self.first_book.refresh_from_db()
        self.assertEqual(1230, self.first_book.price)

    def test_update_not_owner(self):
        url = reverse('book-detail', args=(self.first_book.id,))
        json_data = json.dumps({
            "name": self.first_book.name,
            "price": 1234,
            "author_name": self.first_book.author_name
        })
        self.client.force_login(self.second_user)
        response = self.client.put(url, data=json_data, content_type='application/json')

        self.assertEqual(status.HTTP_403_FORBIDDEN, response.status_code)
        self.assertEqual({'detail': ErrorDetail(string='You do not have permission to perform this action.',
                                                code='permission_denied')}, response.data)
        self.first_book.refresh_from_db()
        self.assertEqual(1000, self.first_book.price)

    def test_update_not_owner_but_staff(self):
        url = reverse('book-detail', args=(self.first_book.id,))
        json_data = json.dumps({
            "name": self.first_book.name,
            "price": 1234,
            "author_name": self.first_book.author_name
        })
        self.client.force_login(self.staff_user)
        response = self.client.put(url, data=json_data, content_type='application/json')

        self.assertEqual(status.HTTP_200_OK, response.status_code)
        self.first_book.refresh_from_db()
        self.assertEqual(1234, self.first_book.price)

    def test_delete(self):
        self.assertEqual(4, Book.objects.all().count())

        url = reverse('book-detail', args=(self.first_book.id,))
        self.client.force_login(self.first_user)
        response = self.client.delete(url)

        self.assertEqual(status.HTTP_204_NO_CONTENT, response.status_code)
        self.assertEqual(3, Book.objects.all().count())

    def test_delete_not_owner(self):
        self.assertEqual(4, Book.objects.all().count())

        url = reverse('book-detail', args=(self.first_book.id,))
        self.client.force_login(self.second_user)
        response = self.client.delete(url)
        self.assertEqual({'detail': ErrorDetail(string='You do not have permission to perform this action.',
                                                code='permission_denied')}, response.data)
        self.assertEqual(status.HTTP_403_FORBIDDEN, response.status_code)
        self.assertEqual(4, Book.objects.all().count())

    def test_delete_not_owner_but_staff(self):
        self.assertEqual(4, Book.objects.all().count())

        url = reverse('book-detail', args=(self.first_book.id,))
        self.client.force_login(self.staff_user)
        response = self.client.delete(url)

        self.assertEqual(status.HTTP_204_NO_CONTENT, response.status_code)
        self.assertEqual(3, Book.objects.all().count())


class BooksRelationTestCase(APITestCase):
    def setUp(self):
        self.first_user = User.objects.create(username='test_username')
        self.second_user = User.objects.create(username='Second test_username')

        self.first_book = Book.objects.create(name='First Test Book', price=1000, author_name='First author',
                                              owner=self.first_user)
        self.second_book = Book.objects.create(name='Second Test Book', price=2050.00, author_name='Second author',
                                               owner=self.first_user)

    def test_like(self):
        url = reverse('userbookrelation-detail', args=(self.first_book.id,))

        json_data = json.dumps({
            "like": True,
        })
        self.client.force_login(self.first_user)
        response = self.client.patch(url, data=json_data, content_type='application/json')

        self.assertEqual(status.HTTP_200_OK, response.status_code)
        relation = UserBookRelation.objects.get(user=self.first_user, book=self.first_book)
        self.assertTrue(relation.like)

    def test_bookmarks(self):
        url = reverse('userbookrelation-detail', args=(self.first_book.id,))

        json_data = json.dumps({
            "in_bookmarks": True,
        })
        self.client.force_login(self.first_user)
        response = self.client.patch(url, data=json_data, content_type='application/json')

        self.assertEqual(status.HTTP_200_OK, response.status_code)
        relation = UserBookRelation.objects.get(user=self.first_user, book=self.first_book)
        self.assertTrue(relation.in_bookmarks)

    def test_rate(self):
        url = reverse('userbookrelation-detail', args=(self.first_book.id,))

        json_data = json.dumps({
            "rate": 3,
        })
        self.client.force_login(self.first_user)
        response = self.client.patch(url, data=json_data, content_type='application/json')

        self.assertEqual(status.HTTP_200_OK, response.status_code)
        relation = UserBookRelation.objects.get(user=self.first_user, book=self.first_book)
        self.assertEqual(3, relation.rate)
