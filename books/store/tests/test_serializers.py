from django.db.models import Count, Case, When, Min, Max, F
from django.test import TestCase
from django.contrib.auth.models import User

from store.models import Book, UserBookRelation
from store.serializers import BookSerializer


class BookSerializerTestCase(TestCase):

    def setUp(self):
        self.first_user = User.objects.create(username='first test_username', first_name='Ivan', last_name='Petrov')
        self.second_user = User.objects.create(username='second test_username', first_name='Petr', last_name='Ivanov')
        self.third_user = User.objects.create(username='third test_username', first_name='Andrey', last_name='Andreev')

        self.first_book = Book.objects.create(name='First Test Book', price=1000, author_name='First author',
                                              discount_plan=Book.STUDENT, owner=self.first_user)
        self.second_book = Book.objects.create(name='Second Test Book', price=2050.00, author_name='Second author',
                                               owner=self.first_user)

        UserBookRelation.objects.create(user=self.first_user, book=self.first_book, like=True, rate=5,
                                        in_bookmarks=True)
        UserBookRelation.objects.create(user=self.second_user, book=self.first_book, like=True, rate=5,
                                        in_bookmarks=True)
        user_book_3 = UserBookRelation.objects.create(user=self.third_user, book=self.first_book, like=True)
        user_book_3.rate = 4
        user_book_3.save()

        UserBookRelation.objects.create(user=self.first_user, book=self.second_book, like=True, rate=3)
        UserBookRelation.objects.create(user=self.second_user, book=self.second_book, like=True, rate=4)
        UserBookRelation.objects.create(user=self.third_user, book=self.second_book, like=False, in_bookmarks=True)

    def test_serializing(self):
        books = Book.objects.all().annotate(
            annotated_likes=Count(Case(When(userbookrelation__like=True, then=1))),
            in_users_bookmarks=Count(Case(When(userbookrelation__in_bookmarks=True, then=1))),
            min_rating=Min('userbookrelation__rate'),
            max_rating=Max('userbookrelation__rate'),
            dicount_price=F('price') - F('price') * F('discount_plan') / 100.00).order_by('id')
        data = BookSerializer(books, many=True).data

        expected_data = [
            {
                'id': self.first_book.id,
                'name': 'First Test Book',
                'price': '1000.00',
                'author_name': 'First author',
                'annotated_likes': 3,
                'rating': '4.67',
                'in_users_bookmarks': 2,
                'min_rating': 4,
                'max_rating': 5,
                'discount_plan': 15,
                'dicount_price': '850.00',
                'owner_name': 'first test_username',
                'readers': [
                    {
                        'username': 'first test_username',
                        'first_name': 'Ivan',
                        'last_name': 'Petrov'
                    },
                    {
                        'username': 'second test_username',
                        'first_name': 'Petr',
                        'last_name': 'Ivanov'
                    },
                    {
                        'username': 'third test_username',
                        'first_name': 'Andrey',
                        'last_name': 'Andreev'
                    }
                ]
            },
            {
                'id': self.second_book.id,
                'name': 'Second Test Book',
                'price': '2050.00',
                'author_name': 'Second author',
                'annotated_likes': 2,
                'rating': '3.50',
                'in_users_bookmarks': 1,
                'min_rating': 3,
                'max_rating': 4,
                'discount_plan': 0,
                'dicount_price': '2050.00',
                'owner_name': 'first test_username',
                'readers': [
                    {
                        'username': 'first test_username',
                        'first_name': 'Ivan',
                        'last_name': 'Petrov'
                    },
                    {
                        'username': 'second test_username',
                        'first_name': 'Petr',
                        'last_name': 'Ivanov'
                    },
                    {
                        'username': 'third test_username',
                        'first_name': 'Andrey',
                        'last_name': 'Andreev'
                    }
                ]
            },
        ]
        self.assertEqual(expected_data, data)
