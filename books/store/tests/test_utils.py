from django.contrib.auth.models import User
from django.test import TestCase

from store.models import UserBookRelation, Book
from store.utils import set_rating


class SetRatingTestCase(TestCase):

    def setUp(self):
        self.first_user = User.objects.create(username='first test_username', first_name='Ivan', last_name='Petrov')
        self.second_user = User.objects.create(username='second test_username', first_name='Petr', last_name='Ivanov')
        self.third_user = User.objects.create(username='third test_username', first_name='Andrey', last_name='Andreev')

        self.first_book = Book.objects.create(name='First Test Book', price=1000, author_name='First author',
                                              discount_plan=Book.STUDENT, owner=self.first_user)

        UserBookRelation.objects.create(user=self.first_user, book=self.first_book, like=True, rate=5,
                                        in_bookmarks=True)
        UserBookRelation.objects.create(user=self.second_user, book=self.first_book, like=True, rate=5,
                                        in_bookmarks=True)
        UserBookRelation.objects.create(user=self.third_user, book=self.first_book, like=True, rate=4)

    def test_ok(self):
        set_rating(self.first_book)
        self.first_book.refresh_from_db()
        self.assertEqual('4.67', str(self.first_book.rating))
