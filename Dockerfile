FROM python:3.9-alpine3.16

COPY requirements.txt /temp/requirements.txt
COPY books /books
COPY entrypoint.sh /temp/entrypoint.sh

WORKDIR /books

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

EXPOSE 8000

RUN apk update \
    && apk add postgresql-dev gcc python3-dev musl-dev

RUN pip install --upgrade pip
RUN pip install -r /temp/requirements.txt

RUN adduser --disabled-password books-user
USER books-user

ENTRYPOINT ["/temp/entrypoint.sh"]